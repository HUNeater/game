#include "Utils.h"
#include <iostream>

std::string Utils::loadResource(std::string path) {
	std::ifstream file;

	try {
		file = std::ifstream(path);
		if (file.fail())
			throw std::exception(("Failed to find " + path).c_str());
		else {
			std::cout << "File " << path << " opened successfully" << std::endl;
		}
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::terminate();
	}

	std::stringstream stream;
	stream << file.rdbuf();
	file.close();

	return stream.str();
}