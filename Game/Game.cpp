//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#define NDEBUG

#include "Game.h"
#include "Utils.h"
#include "Pane.h"
#include "Box.h"
#include "Shaders.h"

#include <assert.h>
#include <SOIL/SOIL.h>

Game::Game() {
	initWindow();
	initGL();
	initGame();
	loop();
}

void Game::initWindow() {
	try {
		if (!glfwInit())
			throw std::exception("Failed to initialize GLFW");
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::terminate();
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	try {
		window = glfwCreateWindow(width, height, TITLE.c_str(), nullptr, nullptr);
		if (window == nullptr)
			throw std::exception("Failed to create GLFW window");
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::terminate();
	}

	glfwMakeContextCurrent(window);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	if (VSYNC) {
		glfwSwapInterval(1);
		std::cout << "Vsync enabled" << std::endl;
	} else {
		glfwSwapInterval(0);
		std::cout << "Vsync disabled" << std::endl;
	}

	if (MSAA) {
		glfwWindowHint(GLFW_SAMPLES, SAMPLES);
		std::cout << "MSAA enabled with " << SAMPLES << " samples" << std::endl;
	} else {
		std::cout << "MSAA disabled" << std::endl;
	}

	try {
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
			throw new std::exception("Failed to initialize GLEW");
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::terminate();
	}

	glfwSetWindowSizeCallback(window, &Game::sizeCallback);
	glfwSetWindowUserPointer(window, this);

	const GLFWvidmode* vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(
		window,
		(vidMode->width - width) / 2,
		(vidMode->height - height) / 2
	);

	glfwShowWindow(window);

	std::cout << "OpenGL initialized" << std::endl;
}

void Game::initGL() {
	glClearColor(
		0 / 255.f, // r
		0 / 255.f, // g
		0 / 255.f, // b
		1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
}

void Game::loop() {
	GLdouble time = glfwGetTime();
	GLdouble fpsTimer = NULL;
	while(!glfwWindowShouldClose(window)) {
		glfwPollEvents();	

		GLdouble timeNow = glfwGetTime();
		GLdouble elapsed = timeNow - time;
		time = timeNow;
		fpsTimer += elapsed;

		// Resizing
		if (resized) {
			glViewport(0, 0, width, height);
			player->getCamera()->resize(width, height);
			resized = false;
			std::cout << "Resized to " << width << "x" << height << std::endl;
		}

		// FPS calculating
		if (fpsTimer >= 1) {
			GLint fps = (GLint) round(1 / elapsed);
			fpsTimer = NULL;
			glfwSetWindowTitle(window, (TITLE + " | fps: " + std::to_string(fps)).c_str());
		}

		tick(elapsed);
		//assert(glGetError() == GL_NO_ERROR);

		render(elapsed);
		//assert(glGetError() == GL_NO_ERROR);

		glfwSwapBuffers(window);
	}

	glfwTerminate();
}

void Game::render(GLdouble delta) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Shaders::getInstance().getObjectShader()->use();
	for_each(objects.begin(), objects.end(), [=](Object* object) {
		object->render(delta);
	});
}

void Game::tick(GLdouble delta) {
	if (input->isKeyPressed(GLFW_KEY_ESCAPE))
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void Game::sizeCallback(GLFWwindow* window, GLint width, GLint height) {
	Game* p = (Game*) glfwGetWindowUserPointer(window);

	p->resized = true;
	p->width = width;
	p->height = height;
}

void Game::initGame() {
	input = new Input(window);

	player = new Player(input, 0.f, 0.5f, 0.f);
	player->setVisible(GL_FALSE);

	GLint width, height;
	unsigned char* image = SOIL_load_image("C:/Users/hunea/Desktop/tex.png", &width, &height, 0, SOIL_LOAD_RGB);
	if (image == 0)
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );

	pane = new Pane(0.f, -51.f, 30.f, image, width, height);
	pane->setShaded(true);
	pane->setTextureScale(10.f, 10.f);
	pane->setColor(1.0f, 0.0f, 0.0f);
	pane->setRotation(90.f, 1.f, 0.f, 0.f);
	pane->setScale(100.f, 100.f, 1.f);

	SOIL_free_image_data(image);

	box = new Box(5.f, 0.f, -5.0f);
	box->setShaded(true);
	box->setColor(0.0f, 1.0f, 0.0f);

	objects.push_back(player);
	objects.push_back(pane);
	objects.push_back(box);
}

int main(int args, char* argv[]) {
	Game game;
	return 0;
}