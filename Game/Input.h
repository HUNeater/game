#pragma once

#include <GLFW/glfw3.h>

class Input {

public:
	Input(GLFWwindow* window);
	GLboolean isKeyPressed(GLint key);
private:
	static void keyCallback(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint mode);
	static GLboolean keys[1024];
};