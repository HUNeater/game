#pragma once

#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <math.h>

#include "Object.h"

class Camera : public Object {
public:
	Camera(GLfloat x, GLfloat y, GLfloat z) : super(x, y, z) {};

	virtual void tick(GLdouble delta) override;
	void resize(GLint width, GLint height);
	void setTarget(glm::vec3 target);
	void addTarget(GLfloat x, GLfloat y, GLfloat z);
	glm::vec3 getTarget();

private:
	GLint width, height;
	GLfloat fov = 45.f;
	glm::vec3 target = glm::vec3();
	glm::vec3 direction = glm::vec3();
	glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);
	glm::mat4 viewMatrix = glm::mat4();
};