#define NDEBUG

#include "Shaders.h"
#include "Utils.h"

#include <iostream>
#include <assert.h>

Shaders::Shaders() {
	objectShader = new Shader(
		Utils::loadResource("vertexShader.glsl"),
		Utils::loadResource("fragmentShader.glsl")
	);

	assert(glGetError() == GL_NO_ERROR);
	std::cout << "Shaders loaded" << std::endl;
}