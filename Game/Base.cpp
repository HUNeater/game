#include "Base.h"

void Base::uniform(Shader* shader, const GLchar var[], glm::mat4 val) {
	GLint loc = glGetUniformLocation(shader->getId(), var);
	if (loc != -1) {
		shader->use();
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(val));
	}
}

void Base::uniform(Shader* shader, const GLchar var[], glm::vec3 val) {
	uniform(shader, var, val.x, val.y, val.z);
}

void Base::uniform(Shader * shader, const GLchar var[], glm::vec2 val) {
	uniform(shader, var, val.x, val.y);
}

void Base::uniform(Shader* shader, const GLchar var[], GLfloat val) {
	GLint loc = glGetUniformLocation(shader->getId(), var);
	if (loc != -1) {
		shader->use();
		glUniform1f(loc, val);
	}
}

void Base::uniform(Shader * shader, const GLchar var[], GLint val){
	GLint loc = glGetUniformLocation(shader->getId(), var);
	if (loc != -1) {
		shader->use();
		glUniform1i(loc, val);
	}
}

void Base::uniform(Shader* shader, const GLchar var[], GLfloat x, GLfloat y, GLfloat z) {
	GLint loc = glGetUniformLocation(shader->getId(), var);
	if (loc != -1) {
		shader->use();
		glUniform3f(loc, x, y, z);
	}
}

void Base::uniform(Shader* shader, const GLchar var[], GLfloat x, GLfloat y) {
	GLint loc = glGetUniformLocation(shader->getId(), var);
	if (loc != -1) {
		shader->use();
		glUniform2f(loc, x, y);
	}
}