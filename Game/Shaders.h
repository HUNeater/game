#pragma once

#include "Shader.h"

class Shaders {
public:
	static Shaders& getInstance() {
		static Shaders instance;
		return instance;
	}

	Shader* getObjectShader() {
		return objectShader;
	}

	Shaders(Shaders const&) = delete;
	void operator=(Shaders const&) = delete;

private:
	Shaders();
	Shader* objectShader;
};