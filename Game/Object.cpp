#include "Object.h"
#include "Game.h"
#include "Shaders.h"

#include <iostream>
#include <assert.h>

Object::Object(GLfloat x, GLfloat y, GLfloat z) {
	worldLocation.x = x;
	worldLocation.y = y;
	worldLocation.z = z;
}

Object::Object(Input* input, GLfloat x, GLfloat y, GLfloat z) : Object(x, y, z) {
	this->input = input;
}

Object::~Object() {
	cleanUp();
}

void Object::render(GLdouble delta) {
	tick(delta);

	if (bVisible) {
		glPolygonMode(GL_FRONT_AND_BACK, renderMode);

		if (VAO) glBindVertexArray(VAO);
		if (TBO) glBindTexture(GL_TEXTURE_2D, TBO);
		draw();
		if (TBO) glBindTexture(GL_TEXTURE_2D, 0);
		if (VAO) glBindVertexArray(0);
	}
}

void Object::tick(GLdouble delta) {
	Shader* shader = Shaders::getInstance().getObjectShader();

	for_each(childs.begin(), childs.end(), [=](Object* object) {
		object->render(delta);
	});

	if (parent != nullptr)
		setWorldLocation(parent->getWorldLocation());

	if (TBO)
		uniform(shader, "useTexture", GL_TRUE);
	else
		uniform(shader, "useTexture", GL_FALSE);

	modelMatrix = glm::mat4();
	modelMatrix = glm::translate(modelMatrix, worldLocation);
	modelMatrix = glm::translate(modelMatrix, relativeLocation);
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.5f * scale.x, 0.5f * scale.y, 0.f));
	modelMatrix = glm::rotate(modelMatrix, rotation, glm::vec3(rotx, roty, rotz));
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.5f * scale.x, -0.5f * scale.y, 0.f));
	modelMatrix = glm::scale(modelMatrix, scale);

	uniform(shader, "model", modelMatrix);
	uniform(shader, "textureScale", textureScale);
	uniform(shader, "mat.ambient", ambient);
	uniform(shader, "mat.color", color);
	uniform(shader, "mat.specular", 0.5f, 0.5f, 0.5f);
	uniform(shader, "mat.shininess", 32.f);

	if (bShaded)
		uniform(shader, "useFlatShading", GL_FALSE);
	else
		uniform(shader, "useFlatShading", GL_TRUE);
}

void Object::setRenderMode(GLint mode) {
	switch (mode) {
	case GL_FILL:
		renderMode = GL_FILL;
		break;

	case GL_LINE:
		renderMode = GL_LINE;
		break;

	default:
		renderMode = GL_FILL;
	}
}

void Object::setVisible(GLboolean b) {
	bVisible = b;
}

void Object::setShaded(GLboolean b) {
	bShaded = b;
}

void Object::setColor(GLfloat r, GLfloat g, GLfloat b) {
	color.x = r;
	color.y = g;
	color.z = b;

	ambient = color * 0.5f;
}

void Object::setScale(GLfloat x, GLfloat y, GLfloat z) {
	scale.x = x;
	scale.y = y;
	scale.z = z;
}

void Object::setRotation(GLfloat rotation, GLfloat rotx, GLfloat roty, GLfloat rotz) {
	this->rotation = glm::radians(rotation);
	this->rotx = rotx;
	this->roty = roty;
	this->rotz = rotz;
}

void Object::setTextureScale(GLfloat x, GLfloat y) {
	textureScale.x = x;
	textureScale.y = y;
}

void Object::addChild(Object * object) {
	childs.push_back(object);
	object->parent = this;
}

void Object::removeChild(Object * object) {
	childs.remove(object);
	object->parent = nullptr;
}

glm::vec3 Object::getWorldLocation() {
	return worldLocation;
}

void Object::cleanUp() {
	if (VAO)
		glDeleteVertexArrays(1, &VAO);

	if (VBO)
		glDeleteBuffers(1, &VBO);

	if (TBO)
		glDeleteBuffers(1, &TBO);

	if (IBO)
		glDeleteBuffers(1, &IBO);
}

void Object::setWorldLocation(glm::vec3 loc) {
	worldLocation = loc;
}