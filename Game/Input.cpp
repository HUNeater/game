#include "Input.h"
#include <GLFW/glfw3.h>

GLboolean Input::keys[1024];

Input::Input(GLFWwindow* window) {
	glfwSetKeyCallback(window, &Input::keyCallback);
}

GLboolean Input::isKeyPressed(GLint key) {
	if (keys[key])
		return GL_TRUE;

	return GL_FALSE;
}

void Input::keyCallback(GLFWwindow * window, GLint key, GLint scancode, GLint action, GLint mode) {
	if (action == GLFW_PRESS)
		keys[key] = GL_TRUE;
	else if (action == GLFW_RELEASE)
		keys[key] = GL_FALSE;
}