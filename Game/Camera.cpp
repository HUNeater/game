#include "Camera.h"
#include "Game.h"
#include "Shaders.h"

void Camera::tick(GLdouble delta) {
	super::tick(delta);

	Shader* shader = Shaders::getInstance().getObjectShader();

	GLfloat rad = 1.f;
	GLfloat speed = 1.f;

	glm::vec3 f;
	f.x = (GLfloat)sin(target.x);
	f.z = (GLfloat)cos(target.z);

	direction = worldLocation + glm::normalize(f);
	viewMatrix = glm::lookAt(worldLocation, direction, up);

	uniform(shader, "view", viewMatrix);
	uniform(shader, "viewPos", worldLocation);

	//glm::mat4 v = glm::mat4(glm::mat3(viewMatrix));
	//uniform(GameScreen.skyboxShader, "view", v);
}

void Camera::resize(GLint width, GLint height) {
	Shader* shader = Shaders::getInstance().getObjectShader();

	this->width = width;
	this->height = height;

	glm::mat4 projection = glm::perspective(fov, (GLfloat)width / (GLfloat)height, 0.1f, 100.f);
	uniform(shader, "projection", projection);
}

void Camera::setTarget(glm::vec3 target) {
	this->target = target;
}

void Camera::addTarget(GLfloat x, GLfloat y, GLfloat z) {
	target.x += x;
	target.z += z;
}

glm::vec3 Camera::getTarget() {
	return direction;
}
