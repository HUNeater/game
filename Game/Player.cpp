#include "Player.h"
#include "PointLight.h"

Player::Player(Input* input, GLfloat x, GLfloat y, GLfloat z) : super(input, x, y, z) {
	camera = new Camera(x, y, z);
	camera->setVisible(GL_FALSE);

	PointLight* light = new PointLight();

	addChild(camera);
	addChild(light);
}

Camera * Player::getCamera() {
	return camera;
}

void Player::tick(GLdouble delta) {
	super::tick(delta);

	// move
	GLdouble moveSpeed = 4.0;
	if (input->isKeyPressed(GLFW_KEY_W) || input->isKeyPressed(GLFW_KEY_UP))
		moveForward(1.f * delta * moveSpeed);

	if (input->isKeyPressed(GLFW_KEY_S) || input->isKeyPressed(GLFW_KEY_DOWN))
		moveForward(-1.f * delta * moveSpeed);

	// turn
	GLdouble turnRate = 2.0;
	if (input->isKeyPressed(GLFW_KEY_D) || input->isKeyPressed(GLFW_KEY_RIGHT))
		turnRight(-1.f * delta * turnRate);

	if (input->isKeyPressed(GLFW_KEY_A) || input->isKeyPressed(GLFW_KEY_LEFT))
		turnRight(1.f * delta * turnRate);
}

void Player::turnRight(GLfloat d) {
	camera->addTarget(d, 0, d);
}

void Player::moveForward(GLfloat f) {
	worldLocation += getForwardVector() * f;
}

glm::vec3 Player::getForwardVector() {
	return glm::normalize(camera->getTarget() - worldLocation);
}