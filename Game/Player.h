#pragma once

#include "Camera.h"
#include "Object.h"

class Player : public Object {
public:
	Player(Input* input, GLfloat x, GLfloat y, GLfloat z);

	Camera* getCamera();
	virtual void tick(GLdouble delta) override;

private:
	void turnRight(GLfloat d);
	void moveForward(GLfloat f);
	glm::vec3 getForwardVector();

	Camera* camera;
};