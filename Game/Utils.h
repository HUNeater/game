#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <string>
#include <sstream>
#include <fstream>

class Utils {
public:
	static std::string loadResource(std::string file);
};