#pragma once

#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <list>

#include "Base.h"
#include "Input.h"

class Object : public Base {
public:
	typedef Object super;
	Object() : Object(0.f, 0.f, 0.f) {};
	Object(GLfloat x, GLfloat y, GLfloat z);
	Object(Input* input, GLfloat x, GLfloat y, GLfloat z);
	~Object();

	void render(GLdouble delta);

	void setVisible(GLboolean b);
	void setRenderMode(GLint mode);
	void setShaded(GLboolean b);
	void setColor(GLfloat r, GLfloat g, GLfloat b);
	void setScale(GLfloat x, GLfloat y, GLfloat z);
	void setRotation(GLfloat rotation, GLfloat rotx, GLfloat roty, GLfloat rotz);
	void setTextureScale(GLfloat x, GLfloat y);
	void addChild(Object* object);
	void removeChild(Object* object);
	glm::vec3 getWorldLocation();

protected:
	void cleanUp();
	void setWorldLocation(glm::vec3 loc);

	virtual void tick(GLdouble delta);
	virtual void draw() {};

	std::list<Object*> childs;
	Object* parent;

	GLuint VAO = 0;
	GLuint VBO = 0;
	GLuint TBO = 0;
	GLuint IBO = 0;
	GLfloat rotation = 90.f;
	GLfloat rotx = 1.f;
	GLfloat roty = 1.f;
	GLfloat rotz = 1.f;
	glm::vec3 worldLocation = glm::vec3();
	glm::vec3 relativeLocation = glm::vec3();
	glm::vec3 color = glm::vec3(1.f, 1.f, 1.f);
	glm::vec3 ambient = glm::vec3();
	glm::vec3 scale = glm::vec3(1.f, 1.f, 1.f);
	glm::vec2 textureScale = glm::vec2(1.f, 1.f);
	glm::mat4 modelMatrix = glm::mat4();
	GLboolean bVisible = true;
	GLboolean bShaded = true;
	Input* input;

private:
	GLint renderMode = GL_FILL;
};