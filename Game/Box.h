#pragma once

#include "Object.h"

class Box : public Object {
public:
	Box(GLfloat x, GLfloat y, GLfloat z);

	virtual void draw() override;
};