#include "PointLight.h"
#include "Shaders.h"

PointLight::PointLight() {
	GLfloat verts[] {
		0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
			-0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
			-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
			0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f
	};

	GLint inds[] {
		0, 1, 2,
		0, 2, 3
	};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &IBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(inds), inds, GL_STATIC_DRAW);

	// vertices
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)12);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	bVisible = false;
}

void PointLight::tick(GLdouble delta) {
	super::tick(delta);

	Shader* shader = Shaders::getInstance().getObjectShader();

	uniform(shader, "light.pos", worldLocation);
	uniform(shader, "light.ambient", color);
	uniform(shader, "light.diffuse", color);
	uniform(shader, "light.specular", 0.5f, 0.5f, 0.5f);
	uniform(shader, "light.constant", 1.f);
	uniform(shader, "light.linear", 0.045f);
	uniform(shader, "light.quadratic", 0.0075f);
}