#pragma once

#include "Object.h"

class Pane : public Object {
public:
	Pane() : Pane(0.f, 0.f, 0.f) {};
	Pane(GLfloat x, GLfloat y, GLfloat z);
	Pane(GLfloat x, GLfloat y, GLfloat z, unsigned char* img, GLsizei width, GLsizei height);

	virtual void draw() override;
};