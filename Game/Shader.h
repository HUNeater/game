#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <string>
#include <stdexcept>
#include <iostream>
#include <vector>

class Shader {
public:
	Shader(std::string vxCode, std::string frCode);
	~Shader();

	void use();
	void unuse();
	void cleanUp();
	void print(std::string text, GLint id);
	GLint getId();

private:
	GLint programID, vertexID, fragmentID;
	GLboolean bUsing = false;
	GLint createShader(const GLchar* code, GLint type);
	GLboolean ready = false;
};