#define NDEBUG

#include "Shader.h"
#include <assert.h>

Shader::Shader(std::string vxCode, std::string frCode) {
	try {
		programID = glCreateProgram();
		if (programID == NULL)
			throw std::exception("Failed to create shader program");
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::terminate();
	}

	vertexID = createShader(vxCode.c_str(), GL_VERTEX_SHADER);
	fragmentID = createShader(frCode.c_str(), GL_FRAGMENT_SHADER);

	// linking
	glLinkProgram(programID);

	GLint linked = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
		print("Failed to link program", programID);

	// validating
	glValidateProgram(programID);

	GLint validated = 0;
	glGetProgramiv(programID, GL_VALIDATE_STATUS, &validated);
	if (validated == GL_FALSE)
		print("Failed to validate program", programID);

	ready = true;
}

Shader::~Shader() {
	cleanUp();
}

void Shader::cleanUp() {
	unuse();

	if (programID != NULL) {
		if (vertexID != NULL)
			glDetachShader(programID, vertexID);

		if (fragmentID != NULL)
			glDetachShader(programID, fragmentID);

		glDeleteProgram(programID);
	}
}

void Shader::print(std::string text, GLint id) {
	GLint length = 0;
	glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);

	if (length > 0) {
		std::vector<GLchar> infoLog(length);
		glGetShaderInfoLog(id, length, &length, &infoLog[0]);

		glDeleteShader(id);

		std::cerr << text << ": " << std::endl;;
		for (std::vector<GLchar>::const_iterator i = infoLog.begin(); i != infoLog.end(); ++i)
			std::cerr << *i;

		std::terminate();
	}
}

GLint Shader::createShader(const GLchar* code, GLint type) {
	GLint shaderId;

	try {
		shaderId = glCreateShader(type);
		if (shaderId == NULL)
			throw std::exception(("Failed to create shader [id=" + std::to_string(shaderId) + "]").c_str());
	} catch (std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::terminate();
	}

	// compiling
	glShaderSource(shaderId, 1, &code, NULL);
	glCompileShader(shaderId);

	GLint compiled = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
		print("Failed to compile shader [id=" + std::to_string(shaderId) + "]", shaderId);

	glAttachShader(programID, shaderId);

	assert(glGetError() == GL_NO_ERROR);
	return shaderId;
}

void Shader::use() {
	if (!bUsing) {
		glUseProgram(programID);
		bUsing = true;
	}
}

void Shader::unuse() {
	if (bUsing) {
		glUseProgram(0);
		bUsing = false;
	}
}

GLint Shader::getId() {
	if (ready) {
		return programID;
	}

	return NULL;
}