#pragma once

#define GLEW_STATIC

#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <algorithm>

#include "Object.h"
#include "Player.h"
#include "Input.h"

class Game {
public:
	Game();

private:
	static void sizeCallback(GLFWwindow* window, GLint width, GLint height);

	void initWindow();
	void initGL();
	void initGame();
	void loop();
	void render(GLdouble delta);
	void tick(GLdouble delta);

	GLuint width = 1280;
	GLuint height = 720;
	GLboolean resized = false;
	GLFWwindow* window;
	std::string TITLE = "OpenGL Game";
	std::list<Object*> objects;
	Object* pane;
	Object* box;
	Player* player;
	Input* input;

	const GLboolean VSYNC = true;
	const GLboolean MSAA = true;
	const GLint SAMPLES = 4;
};