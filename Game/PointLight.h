#pragma once

#include "Object.h"

class PointLight : public Object{
public:
	PointLight();

	virtual void tick(GLdouble delta) override;
};